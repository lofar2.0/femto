
from opcua import Server, ua, uamethod
from datetime import datetime

idx = None
# Store the monitor_points in this dict
monitor_points = {}

@uamethod
def datapoint_change_callback(idx, current_event: dict):
    """
    TODO do your OPC magic here
    :param current_event:
    ["rcu"]=rcu
    ["rcumode"]=rcumode
    ["obs_id"]=obs_id
    ["error"]=error
    ["start_freq"]=start_freq
    ["end_freq"]=end_freq
    ["timestamp"]=timestamp
    ['average_spectrum']=average_spectrum (list of float[256])
    ['spectrum']=spectrum of the bad rcu
    :return:
    """
    global monitor_points
    for key, value in current_event.items():
        monitor_points["RCU_error_spectra"][key]["variable"].set_value(value)

    return True

def setup_server(url: str):
    global monitor_points, idx
    # setup our server
    server = Server()

    server_url = "localhost:55555"
    if url is not None:
        server_url = url

    server.set_endpoint("opc.tcp://" + server_url + "/LOFAR2.0/FEMTO")
    server.set_server_name("LOFAR2.0 Faulty Element Measurement To Opc UA (FEMTO)")
    server.set_security_policy([ua.SecurityPolicyType.NoSecurity,])
    # setup our own namespace, not really necessary but should as spec
    uri = "http://lofar.eu"
    idx = server.register_namespace(uri)

    # Get Objects node.
    objects = server.get_objects_node()

    # Now add some folders.
    folder = objects.add_folder(idx, "FEMTO")

    #Add variables and methods to the subfolder.
    mp_name = "RCU_error_spectra"
    obj = folder.add_object(idx, mp_name)

    var = obj.add_variable(idx, "rcu", 0, ua.VariantType.UInt32)
    # Set to be read-only
    var.set_read_only()
    #var.set_writable()
    # Enable value history.
    # Keep values not older than "period" and
    # keep a maximum of "count" values.
    #server.historize_node_data_change(var, period = datetime.timedelta(hours = 1), count = 10)
    # Add the variable to my bookkeeping dict.
    monitor_points[mp_name]={}
    monitor_points[mp_name]["rcu"] = {"object": obj, "variable": var}

    var = obj.add_variable(idx, "obs_id", ua.Variant(0, ua.VariantType.UInt64))
    # Set to be read-only
    var.set_read_only()
    monitor_points[mp_name]["obs_id"] = {"object": obj, "variable": var}

    var = obj.add_variable(idx, "rcumode", ua.Variant(0, ua.VariantType.UInt64))
    # Set to be read-only
    var.set_read_only()
    monitor_points[mp_name]["rcumode"] = {"object": obj, "variable": var}

    var = obj.add_variable(idx, "error", ua.Variant("", ua.VariantType.String))
    # Set to be read-only
    var.set_read_only()
    monitor_points[mp_name]["error"] = {"object": obj, "variable": var}

    var = obj.add_variable(idx, "start_freq", ua.Variant(0.0, ua.VariantType.Double))
    # Set to be read-only
    var.set_read_only()
    monitor_points[mp_name]["start_freq"] = {"object": obj, "variable": var}

    var = obj.add_variable(idx, "end_freq", ua.Variant(0.0, ua.VariantType.Double))
    # Set to be read-only
    var.set_read_only()
    monitor_points[mp_name]["end_freq"] = {"object": obj, "variable": var}

    var = obj.add_variable(idx, "timestamp", ua.Variant(datetime.utcnow(), ua.VariantType.DateTime))
    # Set to be read-only
    var.set_read_only()
    monitor_points[mp_name]["timestamp"] = {"object": obj, "variable": var}

    empty_list=[0] * 256
    var = obj.add_variable(idx, "average_spectrum", ua.Variant(empty_list, ua.VariantType.Double))
    # Set to be read-only
    var.set_read_only()
    monitor_points[mp_name]["average_spectrum"] = {"object": obj, "variable": var}

    var = obj.add_variable(idx, "spectrum", ua.Variant(empty_list, ua.VariantType.Double))
    # Set to be read-only
    var.set_read_only()
    monitor_points[mp_name]["spectrum"] = {"object": obj, "variable": var}

    return server
