import logging
import os
import re
from io import TextIOBase, StringIO

import platform
if platform.system() == "Linux":
    import inotify.adapters
else:
    print("Cannot import inotify on non-Linux systems.  You may want to consider enabling the simulation.")

from .femto_opc import datapoint_change_callback, idx

logger = logging.getLogger('femto_watchdog')

OPENED_FILES = dict()
data=list()


def instantiate_file_creation_event_observer(path):
    """
    Create an observer object that listens to creation events of station test
    and RTSM in a specific dir and sends the content of these files to the REST
    api of the MaintenanceDB at the given address
    :param path: the dir path to watch for events
    :return: the observer object
    """
    inotify_adapter = inotify.adapters.Inotify()
    inotify_adapter.add_watch(path)
    return inotify_adapter


def main_wait_event_loop(observer, file_name, active_watch_time, simulation, run_once=False):
    """
    Wait for any exception raised
    if it receives a keyboard interrupt it stops the listener
    :param observer: inotify interface observer
    :param expected_filename: regex that describes the expected filename
    :param active_watch_time: time of active watching of inotify events
    :param simulation:  True if the simulated data should be fed to the OPC-UA device.
    :param run_once: watch for inotify events only once and timeout after
     active_watch_time
    """
    if simulation is True:
        from time import sleep
        # Remember the max index of the simulation data array
        max_index = len(data) - 1
        # And begin at a random index
        from random import randint
        index = randint(0, max_index)

    while True:
        if simulation is False:
            try:
                events = observer.event_gen(yield_nones=False,
                    timeout_s=active_watch_time)
                for event in events:
                    handle_inotify_events(event, expected_filename)
            except KeyboardInterrupt:
                logger.info("Keyboard interrupt. Exiting main listener")
                return True
            except Exception as e:
                logger.exception(e)
                logger.warn('Unexpected exception occurred: %s. \n Exiting main listener', e)
                return False
        else:
            sleep(active_watch_time)
            data_string = data[index][0] + data[index][1] + data[index][2]
            event = StringIO(data_string)
            handle_event(event)
            if observer.is_set():
                break
            else:
                index = index + 1
                if index > max_index:
                    index = 0
        if run_once:
            break


def handle_inotify_events(event, expected_filename,):
    """
    On inotify event handler
    :param event: the list that describes the creation event
    """
    global OPENED_FILES
    (_, event_type, path, filename) = event
    filepath = os.path.join(path, filename)

    if _is_file_created(event_type) and _does_filename_matches_specified_regex(expected_filename, event):
        OPENED_FILES[filepath] = open(filepath, 'r')
        logger.debug('Opening file for event: %s', event)
    elif _is_file_closed(event_type) and _does_filename_matches_specified_regex(expected_filename, event):

        if filepath in OPENED_FILES:
            logger.debug('Handling event before closing: %s', event)
            handle_event(OPENED_FILES[filepath])
            logger.debug('Closing file for event: %s', event)
            OPENED_FILES[filepath].close()
            OPENED_FILES.pop(filepath)

    elif _is_file_written(event_type) and _does_filename_matches_specified_regex(expected_filename, event):
        if filepath not in OPENED_FILES:
            OPENED_FILES[filepath] = open(filepath, 'r')
        logger.debug('Handling event: %s', event)
        handle_event(OPENED_FILES[filepath])
    else:
        logger.debug("Neglecting uninteresting event : %s", event)


def _does_filename_matches_specified_regex(regex, event):
    """
    Checks whether the file name of the event matched the specified regex
    :param regex: desired regex
    :param event: inotify event
    :return: true if the file name matches the regex
    """
    return bool(re.match(regex, event[3]))


def _is_file_created(event):
    """
    Checks if the file is created
    :param event: the list that describes the creation event
    :return: True if it refers to a file being created False otherwise
    """
    return 'IN_CREATE' in event[0]


def _is_file_written(event):
    """
    Checks if the file it is modified
    :param event: the list that describes the creation event
    :return: True if it refers to a file being created False otherwise
    """
    return 'IN_MODIFY' in event[0]


def _is_file_closed(event):
    """
    Checks if the file has been closed
    :param event: the list that describes the creation event
    :return: True if it refers to a file being created False otherwise
    """
    return 'IN_CLOSE_WRITE' in event[0] or 'IN_CLOSE_NOWRITE' in event[0]


def _is_line_interesting(line: str):

    is_it = not line.startswith('#')
    is_it *= not line.startswith('OBS')
    is_it *= not (line is '')
    return is_it


def is_line_metadata(line: str):
    return line.startswith('SPECTRA-INFO=')

def is_line_bad_spectra(line: str):
    return line.startswith('BAD-SPECTRA=')

def is_line_average_spectra(line: str):
    return line.startswith('MEAN-SPECTRA=')

def parse_spectra(line: str):
    logger.debug('parsing spectra')
    spectra = [value for value in line.split('=')[1].strip().lstrip('[').rstrip(']').split(' ') if value]
    logger.debug('spectrum %s', spectra)
    spectra = [float(value) for value in line.split('=')[1].strip().lstrip('[').rstrip(']').split(' ') if value]
    return spectra

def parse_metadata(line: str):
    """
    parses this
    rcu,rcumode,obs-id,check,startfreq,stopfreq,rec-timestamp
    SPECTRA-INFO=5,5,641999,SN,100,200,1520211605.115792
    :param line:
    :return:
    """
    rcu, \
    rcumode,\
    obs_id, \
    error, \
    start_freq, \
    end_freq, \
    timestamp = line.split('=')[1].strip().split(',')

    rcu = int(rcu)
    rcumode = int(rcumode)
    obs_id = int(obs_id)
    start_freq = float(start_freq)
    end_freq = float(end_freq)
    timestamp = float(timestamp)
    return dict(rcu=rcu, rcumode=rcumode, obs_id=obs_id, error=error, start_freq=start_freq, end_freq=end_freq, timestamp=timestamp)



def handle_event(f_stream: TextIOBase):
    """
    Handle the file created event sending the file to the MaintenanceDB REST API.
    If the file content doesnt match a RTSM or a station tests, it logs the event and skips
    the insertion.

    :param event: inotify event list description
    """

    current_event = {}
    for line in f_stream.readlines():
        line = line.rstrip('\n').strip()
        if _is_line_interesting(line):
            if is_line_metadata(line):
                current_event = parse_metadata(line)
            elif is_line_average_spectra(line):
                current_event['average_spectrum'] = parse_spectra(line)
            elif is_line_bad_spectra(line):
                # THIS is the finish of the event
                current_event['spectrum'] = parse_spectra(line)
                datapoint_change_callback(idx, current_event)
                current_event = {}
    return None


def read_simulation_data(file_name):
    try:
        f=open(file_name, "r")
        lines=f.readlines()
        f.close()

        d=list()
        for l in lines:
            if is_line_metadata(l) is True:
                d.append(lines.index(l))

        global data
        for index in d:
            info=lines[index]
            mean=lines[index+1]
            spec=lines[index+2]
            data.append([info,mean,spec])
    except Exception as e:
        logger.error("Cannot read simulation data from file %s." % (file_name))
