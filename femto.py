#! /usr/bin/env python3


import sys
import logging


from lib.femto_watchdog import instantiate_file_creation_event_observer, \
    read_simulation_data, main_wait_event_loop
from argparse import ArgumentParser
from lib.femto_opc import setup_server

MAIN_LOOP_WAIT_TIME = 1

# Store the call_methods in this dict
methods = {}
path = ""
file_name = ""
simulation = False


def setup_logging_framework(logging_level=logging.INFO):
    """
    Setup the logging style for a desired logging_level
    :param logging_level: logging level lower of which the messages will be
    filtered out
    """
    logging.basicConfig(format="%(asctime)s %(levelname)s: %(message)s",
                        level=logging_level)


def get_settings_from_command_arguments(args):
    """
    Get the settings from the the command arguments
    :param args: command line arguments
    :return: the station_test_watchdog settings
    """
    argument_parser = setup_command_argument_parser()
    settings = argument_parser.parse_args(args)
    return settings


def setup_command_argument_parser():
    """
    Setup the command arguments parser with the station_test_watchdog different
    options and returns it
    :return: ArgumentParser with the station_test_watchdog options
    """
    parser = ArgumentParser(description="The FEMTO watchdog (FEMTO:  LOFAR2.0 Faulty Element Measurement To Opc UA) monitors files that are written to.  Start it like this:  ./femto.py 127.0.0.1:55555 test/ \"foo.txt\" and it will start an OPC UA server on the host's local interface on the private port 55555.  As soon as the contents of the file \"test/foo.txt\" are modified, the underlying monitoring library will parse the file contents and provide the resulting data as OPC UA monitor points.")
    parser.add_argument('--url', '-u', dest='url', help="The OPC UA server's IP address or hostname and the port, both separated by a colon.  Example:  \"127.0.0.1:55555\"", default="127.0.0.1:55555", required=False)
    parser.add_argument('--file', '-f', dest='file_name', help="The file system's path of the monitored file(s) or the simulation data file.  The file or files that will be monitored must exist in this directory!  File name(s) - not the paths! - can contain regular expressions.  Examples:  \"test/foo.txt\" or \"bar/fo*.txt\"", default="test/test_rtsm_file", required=True)
    parser.add_argument('--simulation', '-s', dest='simulation', help='Switch on dynamic simulation.  Data will be read from the file in the test directory and the entries be randomly selected to update the monitor points every %fs.' % (MAIN_LOOP_WAIT_TIME), default=True, required=False)
    parser.add_argument('--verbose,-v', dest='verbose', help='Switch on verbose logging.', action='store_true', default=False, required=False)
    return parser


def apply_global_settings(settings):
    """
    Apply the settings to the global variables (e.g. the logging level)
    :param settings: command line argument settings
    :return:
    """
    global path, file_name, simulation
    simulation = settings.simulation
    path, file_name = settings.file_name.rsplit("/", 1)
    if len(path) == 0:
        # The file path does not contain a '/'.  Add one.
        path="./"


def femto(args):
    """
    Main function
    :param args: command line arguments
    """
    global MAIN_LOOP_WAIT_TIME
    global path, file_name, simulation

    settings = get_settings_from_command_arguments(args)

    log_level = logging.INFO
    if settings.verbose is True:
        log_level = logging.DEBUG

    setup_logging_framework(log_level)
    apply_global_settings(settings)
    server = setup_server(settings.url)

    try:
        # Start the OPC-UA server.
        server.start()
        if simulation is False:
            observer = instantiate_file_creation_event_observer(path)
            main_wait_event_loop(observer, file_name, MAIN_LOOP_WAIT_TIME, simulation)
        else:
            read_simulation_data(settings.file_name)
            import threading
            observer = threading.Event()
            main_loop = threading.Thread(target = main_wait_event_loop, args = (observer, file_name, MAIN_LOOP_WAIT_TIME, simulation))
            main_loop.start()
            main_loop.join()
    except KeyboardInterrupt:
        if simulation is True:
            observer.set()
            main_loop.join()
    except Exception as e:
        logging.exception(e)
    finally:
        #close connection, remove subcsriptions, etc
        server.stop()

if __name__ == "__main__":
    femto(sys.argv[1:])
